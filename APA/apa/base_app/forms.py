from django import forms
from django.contrib.auth.models import User
from base_app.models import UserProfileInfo, InfoForm
from django.forms import ModelForm
from django.core.validators import MaxValueValidator
from django.views.generic.edit import DeleteView


class UserForm(forms.ModelForm):

    password = forms.CharField(widget=forms.PasswordInput())
    class Meta():
        model = User
        fields = ('username', 'email', 'password')

class FormName(forms.ModelForm):
    # username = forms.CharField(max_length=256)
    # email = forms.EmailField(max_length=256)
    # user_id = forms.IntegerField(primary_key=True)
    # number = forms.IntegerField()
    # username = forms.CharField(max_length=256)
    # email = forms.EmailField(max_length=256)
    
   
    class Meta:
        model = InfoForm
        fields = ('username', 'email', 'number')
        

from django.db import models
from django.contrib.auth.models import User


# Create your models here.

class UserProfileInfo(models.Model):

    user = models.OneToOneField(User)

    def __str__(self):
        return self.user.username

class InfoForm(models.Model):

    # user = models.OneToOneField(User, default='user')
    # user = models.OneToOneField(User)
    username = models.CharField(max_length=256)
    email = models.EmailField(max_length=256)
    number = models.PositiveIntegerField()
    # phone = models.CharField(max_length=256, blank=True, null=True)
    # username = models.CharField(max_length=256)
    # email = models.CharField(max_length=256)
    # username = models.CharField(max_length=256)
    # email = models.EmailField(max_length=256)

    def __str__(self):
        return self.user.username

from django.shortcuts import render
from base_app.forms import UserForm, FormName
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from base_app.models import User, InfoForm


@login_required
def main(request): #Function added
    print(username)
    return render(request, 'base_app/index.html', {'username':username})

def index(request): # Function changed
    return render(request, 'registration/login.html')

@login_required
def user_logout(request): #Function added
    logout(request)
    return HttpResponseRedirect(reverse('index'))

def register(request):

    registered = False
    if request.method == 'POST':
        user_form = UserForm(data=request.POST)

        if user_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()

            registered = True
        else:
            print(user_form.errors)
    else:
        user_form = UserForm()

    return render(request, 'registration/registration.html', {'user_form':user_form, 'registered':registered})

username = None
def user_login(request):
    if request.method == 'POST':
        global username
        username = request.POST.get('username')
        print(username)
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)

        if user:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect(reverse('main')) # linde changed

            else:
                return HttpResponse("ACCOUNT NOT ACTIVE")

        else:
            print("Someone tried to login and failed!")
            print("Username: {} and password: {}".format(username, password))
            # messages.info(request, 'Wrong Username or Password! Try again.')
            # return HttpResponseRedirect(reverse('login'), {'message': 'wrong'})
            return render(request, 'registration/login.html', {'message':'Wrong Username or Password! Try again.'})


    else:
        return render(request, 'registration/login.html', {})

def form(request):

    registered = False

    if request.method == 'POST':
        user_form = FormName(data=request.POST)

        if user_form.is_valid():
            user = user_form.save()
            user.save()

            registered = True

        else:
            print(user_form.errors)
    else:
        user_form = FormName()

    return render(request, 'base_app/form-samples.html', {'user_form':user_form, 'registered':registered})



    return render(request, 'base_app/form-samples.html')

def table(request):
    user_list = InfoForm.objects.order_by('id')
    user_dict = {'access_records':user_list}
        
    return render(request, 'base_app/table-basic.html', context=user_dict)




from django.conf.urls import url, include
from base_app import views
from django.contrib.auth import views as auth_views


app_name = 'base_app'

urlpatterns = [
    url(r'^forms/$', views.form, name='form'),
    url(r'^tables/$', views.table, name='table'),
    url(r'^register/', views.register, name='register'),
    url(r'^login/$', views.user_login, name='login'),
    # url(r'^main/', views.index, name = 'main'),
    url(r'^logout/$', views.user_logout, name='logout'),# line added
]
